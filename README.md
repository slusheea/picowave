# synth0 - Homemade synthesiser
![RP2040](https://img.shields.io/badge/-rp2040-gray?style=for-the-badge&logo=raspberrypi&labelColor=%23bd0840) ![Rust](https://img.shields.io/badge/-rust-gray?style=for-the-badge&logo=rust&labelColor=%23f46623) ![License](https://img.shields.io/badge/MIT%2FApache%202.0%20dual%20license-License-gray?style=for-the-badge&labelColor=blue)

I made this as a learning project, a 6 voice synthesiser with minimal previous knowledge about analog electronics and synths.

![img](https://i.imgur.com/MPOx6tv.jpeg)

Hear the sounds: (click to visit peertube)

[![img](https://peertube.scd31.com/lazy-static/previews/8cc2d42b-d709-451e-9dc9-7474886abbe9.jpg)](https://peertube.scd31.com/w/wmpYDcu4jCGindSHurY7cW)

Read more:
https://slushee.dev/projects/synth0
