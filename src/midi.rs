#![allow(clippy::unusual_byte_groupings)]

use crate::types::{Midi, Sm, StateMachines};
use defmt::info;
use libm::exp2f;

// MIDI message bit masks
/// MIDI messages *usually* consist of three bytes, a status byte and two data bytes. The status
/// byte always starts with a 1 MSB.
pub const STATUS_BIT: u8 = 0b1000_0000;
/// The 4 LSBs of the status byte indicate the MIDI channel the message is addressed to
pub const CHANNEL_BITS: u8 = 0b0000_1111;
/// Besides the status bit, the 3 MSBs indicate which MIDI event occured
pub const EVENT_BITS: u8 = 0b0111_0000;

// MIDI functions/events (that we care about)
// Extracted from https://midi.org/expanded-midi-1-0-messages-list
pub const NOTE_OFF: u8 = 0b0_000_0000;
pub const NOTE_ON: u8 = 0b0_001_0000;
pub const PITCH_BEND: u8 = 0b0_110_0000;
pub const CONTROL_CHANGE: u8 = 0b0_011_0000;
pub const MIDI_EVENTS: [u8; 4] = [NOTE_OFF, NOTE_ON, PITCH_BEND, CONTROL_CHANGE];

pub const MIDI_CHANNEL: u8 = 0;

/// Reads the UART FIFO until either it finds a MIDI status byte or the FIFO is empty
pub fn get_midi_status_byte(midi: &Midi, channel: u8) -> Option<u8> {
    let mut buf = [0];

    while midi.uart_is_readable() {
        if midi.read_raw(&mut buf).is_err() {
            return None;
        }
        // It's a MIDI status message    it's addressed to the channel we're at
        if (buf[0] & STATUS_BIT != 0) && (buf[0] & CHANNEL_BITS == channel) {
            return Some(buf[0]);
        }
    }

    None
}

pub fn note_off(
    [note_number, _]: [u8; 2],
    state_machines: Option<StateMachines>,
    active_notes: &mut [Option<u8>; 6],
) -> Option<StateMachines> {
    info!("Note {} off!", note_number);

    let sm = active_notes.iter().position(|n| n == &Some(note_number));
    if let Some(sm_number) = sm {
        active_notes[sm_number] = None;
    }

    match (sm, state_machines) {
        (Some(0), Some((Sm::Running(sm0), sm1, sm2, sm3, sm4, sm5))) => {
            Some((Sm::Stopped(sm0.stop()), sm1, sm2, sm3, sm4, sm5))
        }
        (Some(1), Some((sm0, Sm::Running(sm1), sm2, sm3, sm4, sm5))) => {
            Some((sm0, Sm::Stopped(sm1.stop()), sm2, sm3, sm4, sm5))
        }
        (Some(2), Some((sm0, sm1, Sm::Running(sm2), sm3, sm4, sm5))) => {
            Some((sm0, sm1, Sm::Stopped(sm2.stop()), sm3, sm4, sm5))
        }
        (Some(3), Some((sm0, sm1, sm2, Sm::Running(sm3), sm4, sm5))) => {
            Some((sm0, sm1, sm2, Sm::Stopped(sm3.stop()), sm4, sm5))
        }
        (Some(4), Some((sm0, sm1, sm2, sm3, Sm::Running(sm4), sm5))) => {
            Some((sm0, sm1, sm2, sm3, Sm::Stopped(sm4.stop()), sm5))
        }
        (Some(5), Some((sm0, sm1, sm2, sm3, sm4, Sm::Running(sm5)))) => {
            Some((sm0, sm1, sm2, sm3, sm4, Sm::Stopped(sm5.stop())))
        }
        (_, s_machines) => {
            /* Unpressed note didn't start playing in the first place */
            s_machines
        }
    }
}

pub fn note_on(
    [note_number, velocity]: [u8; 2],
    state_machines: Option<StateMachines>,
    active_notes: &mut [Option<u8>; 6],
    clock: u32,
) -> Option<StateMachines> {
    info!("Note {} on with velocity {}", note_number, velocity);
    if let Some((Sm::Stopped(mut sm0), sm1, sm2, sm3, sm4, sm5)) = state_machines {
        active_notes[0] = Some(note_number);

        sm0.set_clock_divisor(note_to_pio_div(note_number, clock));

        Some((Sm::Running(sm0.start()), sm1, sm2, sm3, sm4, sm5))
    } else if let Some((sm0, Sm::Stopped(mut sm1), sm2, sm3, sm4, sm5)) = state_machines {
        active_notes[1] = Some(note_number);

        sm1.set_clock_divisor(note_to_pio_div(note_number, clock));

        Some((sm0, Sm::Running(sm1.start()), sm2, sm3, sm4, sm5))
    } else if let Some((sm0, sm1, Sm::Stopped(mut sm2), sm3, sm4, sm5)) = state_machines {
        active_notes[2] = Some(note_number);

        sm2.set_clock_divisor(note_to_pio_div(note_number, clock));

        Some((sm0, sm1, Sm::Running(sm2.start()), sm3, sm4, sm5))
    } else if let Some((sm0, sm1, sm2, Sm::Stopped(mut sm3), sm4, sm5)) = state_machines {
        active_notes[3] = Some(note_number);

        sm3.set_clock_divisor(note_to_pio_div(note_number, clock));

        Some((sm0, sm1, sm2, Sm::Running(sm3.start()), sm4, sm5))
    } else if let Some((sm0, sm1, sm2, sm3, Sm::Stopped(mut sm4), sm5)) = state_machines {
        active_notes[4] = Some(note_number);

        sm4.set_clock_divisor(note_to_pio_div(note_number, clock));

        Some((sm0, sm1, sm2, sm3, Sm::Running(sm4.start()), sm5))
    } else if let Some((sm0, sm1, sm2, sm3, sm4, Sm::Stopped(mut sm5))) = state_machines {
        active_notes[5] = Some(note_number);

        sm5.set_clock_divisor(note_to_pio_div(note_number, clock));

        Some((sm0, sm1, sm2, sm3, sm4, Sm::Running(sm5.start())))
    } else {
        state_machines
    }
}

pub fn pitch_bend(bytes: [u8; 2]) {
    let bend = u16::from_le_bytes(bytes);
    info!("Bending to {}", bend);
}

// f = 440 * 2^((n - 69) / 12)
// Where n is the midi note number and f is the audible frequency,
// assuming A4 = 440Hz
//
// There are 64 bits per wave, and the PIO state machine takes 2 clock cycles to get each bit out
#[inline(always)]
fn note_to_pio_div(note: u8, clock: u32) -> f32 {
    let freq = 440.0 * exp2f((f32::from(note) - 69.0) / 12.0) * 128.0 * 2.0;
    clock as f32 / freq
}
