#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

mod hardware;
mod midi;
mod types;
#[rtic::app(
    device = rp_pico::hal::pac,
    dispatchers = [PWM_IRQ_WRAP]
)]
mod app {
    use bsp::hal::{
        clocks::{init_clocks_and_plls, Clock},
        dma::{self, DMAExt},
        fugit::RateExtU32,
        gpio::FunctionPio0,
        pac,
        pio::{self, PIOExt},
        sio::Sio,
        uart::{DataBits, StopBits, UartConfig, UartPeripheral},
        watchdog::Watchdog,
    };
    use bsp::XOSC_CRYSTAL_FREQ;
    use rp_pico::{self as bsp, hal::gpio::FunctionPio1};

    use defmt::info;
    use defmt_rtt as _;
    use panic_probe as _;

    use cortex_m::singleton as s;
    use rtic_monotonics::rp2040::prelude::*;

    use crate::hardware::*;
    use crate::midi::*;
    use crate::types::*;

    rp2040_timer_monotonic!(Mono);

    #[shared]
    struct Shared {}

    #[local]
    struct Local {
        tx0: Option<DmaTx<dma::CH0, dma::CH1, pac::PIO0, pio::SM0>>,
        tx1: Option<DmaTx<dma::CH2, dma::CH3, pac::PIO0, pio::SM1>>,
        tx2: Option<DmaTx<dma::CH4, dma::CH5, pac::PIO0, pio::SM2>>,
        tx3: Option<DmaTx<dma::CH6, dma::CH7, pac::PIO1, pio::SM0>>,
        tx4: Option<DmaTx<dma::CH8, dma::CH9, pac::PIO1, pio::SM1>>,
        tx5: Option<DmaTx<dma::CH10, dma::CH11, pac::PIO1, pio::SM2>>,
        state_machines: Option<StateMachines>,
        active_notes: [Option<u8>; 6],
        midi: Midi,
        channel: u8,
        system_clock: u32,
    }

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local) {
        info!("Program start");
        let mut watchdog = Watchdog::new(cx.device.WATCHDOG);
        let sio = Sio::new(cx.device.SIO);

        let clocks = init_clocks_and_plls(
            XOSC_CRYSTAL_FREQ,
            cx.device.XOSC,
            cx.device.CLOCKS,
            cx.device.PLL_SYS,
            cx.device.PLL_USB,
            &mut cx.device.RESETS,
            &mut watchdog,
        )
        .ok()
        .unwrap();

        Mono::start(cx.device.TIMER, &cx.device.RESETS);

        let pins = bsp::Pins::new(
            cx.device.IO_BANK0,
            cx.device.PADS_BANK0,
            sio.gpio_bank0,
            &mut cx.device.RESETS,
        );

        let voice_0 = pins.gpio16.into_function::<FunctionPio0>().id().num;
        let voice_1 = pins.gpio17.into_function::<FunctionPio0>().id().num;
        let voice_2 = pins.gpio18.into_function::<FunctionPio0>().id().num;
        let voice_3 = pins.gpio19.into_function::<FunctionPio1>().id().num;
        let voice_4 = pins.gpio20.into_function::<FunctionPio1>().id().num;
        let voice_5 = pins.gpio21.into_function::<FunctionPio1>().id().num;

        let uart_pins = (pins.gpio0.into_function(), pins.gpio1.into_function());

        let mut midi = UartPeripheral::new(cx.device.UART0, uart_pins, &mut cx.device.RESETS)
            .enable(
                // Midi runs at 31250 baud rate, uses 8 data bits, no parity and a single stop bit
                UartConfig::new(31250.Hz(), DataBits::Eight, None, StopBits::One),
                clocks.peripheral_clock.freq(),
            )
            .unwrap();
        midi.enable_rx_interrupt();
        midi.set_fifos(true);

        let program =
            pio_proc::pio_asm!(".wrap_target", "    out x, 1", "    mov pins, x", ".wrap");

        // Initialize and start PIO
        let (mut pio0, sm0, sm1, sm2, _) = cx.device.PIO0.split(&mut cx.device.RESETS);
        let (mut pio1, sm3, sm4, sm5, _) = cx.device.PIO1.split(&mut cx.device.RESETS);
        let installed0 = pio0.install(&program.program).unwrap();
        let installed1 = pio1.install(&program.program).unwrap();
        let (sm0, tx0) = pio_build(voice_0, unsafe { installed0.share() }, sm0);
        let (sm1, tx1) = pio_build(voice_1, unsafe { installed0.share() }, sm1);
        let (sm2, tx2) = pio_build(voice_2, unsafe { installed0.share() }, sm2);
        let (sm3, tx3) = pio_build(voice_3, unsafe { installed1.share() }, sm3);
        let (sm4, tx4) = pio_build(voice_4, unsafe { installed1.share() }, sm4);
        let (sm5, tx5) = pio_build(voice_5, unsafe { installed1.share() }, sm5);

        let dma = enable_dma_irqs(cx.device.DMA.split(&mut cx.device.RESETS));

        info!("Starting tasks");

        (
            Shared {},
            Local {
                tx0: dma_transfer(
                    dma.ch0,
                    dma.ch1,
                    tx0,
                    s!(: [u32; 4] = WAVE),
                    s!(: [u32; 4] = WAVE),
                ),
                tx1: dma_transfer(
                    dma.ch2,
                    dma.ch3,
                    tx1,
                    s!(: [u32; 4] = WAVE),
                    s!(: [u32; 4] = WAVE),
                ),
                tx2: dma_transfer(
                    dma.ch4,
                    dma.ch5,
                    tx2,
                    s!(: [u32; 4] = WAVE),
                    s!(: [u32; 4] = WAVE),
                ),
                tx3: dma_transfer(
                    dma.ch6,
                    dma.ch7,
                    tx3,
                    s!(: [u32; 4] = WAVE),
                    s!(: [u32; 4] = WAVE),
                ),
                tx4: dma_transfer(
                    dma.ch8,
                    dma.ch9,
                    tx4,
                    s!(: [u32; 4] = WAVE),
                    s!(: [u32; 4] = WAVE),
                ),
                tx5: dma_transfer(
                    dma.ch10,
                    dma.ch11,
                    tx5,
                    s!(: [u32; 4] = WAVE),
                    s!(: [u32; 4] = WAVE),
                ),
                state_machines: Some((
                    Sm::Stopped(sm0),
                    Sm::Stopped(sm1),
                    Sm::Stopped(sm2),
                    Sm::Stopped(sm3),
                    Sm::Stopped(sm4),
                    Sm::Stopped(sm5),
                )),
                active_notes: [None; 6],
                midi,
                channel: MIDI_CHANNEL,
                system_clock: clocks.system_clock.freq().to_Hz(),
            },
        )
    }

    #[task(binds = DMA_IRQ_0, local = [tx0, tx1, tx2])]
    fn dma_tranrfer_0(cx: dma_tranrfer_0::Context) {
        let mut tx0 = cx.local.tx0.take().unwrap();
        let mut tx1 = cx.local.tx1.take().unwrap();
        let mut tx2 = cx.local.tx2.take().unwrap();
        if tx0.check_irq0() {
            let (tx_buf, next_tx_transfer) = tx0.wait();
            *cx.local.tx0 = Some(next_tx_transfer.read_next(tx_buf));
        } else {
            _ = cx.local.tx0.insert(tx0);
        }

        if tx1.check_irq0() {
            let (tx_buf, next_tx_transfer) = tx1.wait();
            *cx.local.tx1 = Some(next_tx_transfer.read_next(tx_buf));
        } else {
            _ = cx.local.tx1.insert(tx1);
        }

        if tx2.check_irq0() {
            let (tx_buf, next_tx_transfer) = tx2.wait();
            *cx.local.tx2 = Some(next_tx_transfer.read_next(tx_buf));
        } else {
            _ = cx.local.tx2.insert(tx2);
        }
    }

    #[task(binds = DMA_IRQ_1, local = [tx3, tx4, tx5])]
    fn dma_tranrfer_1(cx: dma_tranrfer_1::Context) {
        let mut tx3 = cx.local.tx3.take().unwrap();
        let mut tx4 = cx.local.tx4.take().unwrap();
        let mut tx5 = cx.local.tx5.take().unwrap();
        if tx3.check_irq1() {
            let (tx_buf, next_tx_transfer) = tx3.wait();
            *cx.local.tx3 = Some(next_tx_transfer.read_next(tx_buf));
        } else {
            _ = cx.local.tx3.insert(tx3);
        }

        if tx4.check_irq1() {
            let (tx_buf, next_tx_transfer) = tx4.wait();
            *cx.local.tx4 = Some(next_tx_transfer.read_next(tx_buf));
        } else {
            _ = cx.local.tx4.insert(tx4);
        }

        if tx5.check_irq1() {
            let (tx_buf, next_tx_transfer) = tx5.wait();
            *cx.local.tx5 = Some(next_tx_transfer.read_next(tx_buf));
        } else {
            _ = cx.local.tx5.insert(tx5);
        }
    }

    #[task(binds = UART0_IRQ, local = [midi, state_machines, active_notes, channel, system_clock])]
    fn read_uart(cx: read_uart::Context) {
        let midi = cx.local.midi;
        let active_notes = cx.local.active_notes;

        // We don't want to leave anything on the fifo!
        while midi.uart_is_readable() {
            let Some(status_byte) = get_midi_status_byte(midi, *cx.local.channel) else {
                break; // We emptied the fifo and got nothing we care about, so we're done
            };

            let event = status_byte & EVENT_BITS;
            if !MIDI_EVENTS.contains(&event) {
                continue; // No need to match the event if we know we don't care about it
            }

            let mut buf = [0; 2];
            midi.read_raw(&mut buf).unwrap();

            match event {
                NOTE_OFF => {
                    *cx.local.state_machines =
                        note_off(buf, cx.local.state_machines.take(), active_notes);
                }
                NOTE_ON => {
                    *cx.local.state_machines = note_on(
                        buf,
                        cx.local.state_machines.take(),
                        active_notes,
                        *cx.local.system_clock,
                    );
                }
                CONTROL_CHANGE => {
                    todo!()
                }
                PITCH_BEND => pitch_bend(buf),
                _ => { /* What!? */ }
            }
        }
    }
}
