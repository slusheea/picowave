use rp_pico::{
    hal::{
        dma::{
            double_buffer::{ReadNext, Transfer},
            Channel,
        },
        gpio::{
            bank0::{Gpio0, Gpio1},
            FunctionUart, Pin, PullDown,
        },
        pac::UART0,
        pio::{PIOExt, Running, StateMachine, StateMachineIndex, Stopped, Tx, SM0, SM1, SM2},
        uart::{self, UartPeripheral},
    },
    pac::{PIO0, PIO1},
};

pub type Midi = UartPeripheral<
    uart::Enabled,
    UART0,
    (
        Pin<Gpio0, FunctionUart, PullDown>,
        Pin<Gpio1, FunctionUart, PullDown>,
    ),
>;

pub enum Sm<PIO: PIOExt, SM: StateMachineIndex> {
    Running(StateMachine<(PIO, SM), Running>),
    Stopped(StateMachine<(PIO, SM), Stopped>),
}

pub type StateMachines = (
    Sm<PIO0, SM0>,
    Sm<PIO0, SM1>,
    Sm<PIO0, SM2>,
    Sm<PIO1, SM0>,
    Sm<PIO1, SM1>,
    Sm<PIO1, SM2>,
);

pub type DmaTx<CH0, CH1, PIO, SM> = Transfer<
    Channel<CH0>,
    Channel<CH1>,
    &'static mut [u32; 4],
    Tx<(PIO, SM)>,
    ReadNext<&'static mut [u32; 4]>,
>;
