use crate::types::DmaTx;
use rp_pico::hal::{
    dma::{double_buffer::Config, Channel, ChannelIndex, Channels, SingleChannel},
    pio::{
        InstalledProgram, PIOBuilder, PIOExt, PinDir, StateMachine, StateMachineIndex, Stopped, Tx,
    },
};

pub fn enable_dma_irqs(mut dma: Channels) -> Channels {
    dma.ch0.enable_irq0();
    dma.ch1.enable_irq0();
    dma.ch2.enable_irq0();
    dma.ch3.enable_irq0();
    dma.ch4.enable_irq0();
    dma.ch5.enable_irq0();
    dma.ch6.enable_irq1();
    dma.ch7.enable_irq1();
    dma.ch8.enable_irq1();
    dma.ch9.enable_irq1();
    dma.ch10.enable_irq1();
    dma.ch11.enable_irq1();
    dma
}

// The LSBs get shifted out first!!
#[allow(clippy::unusual_byte_groupings)]
pub const WAVE: [u32; 4] = [
    0b11111111111110111110111011010110,
    0b01010111011011111101111111111111,
    0b00000000000000001000010010010101,
    0b01010010010001000000000010000000,
];

pub fn dma_transfer<CH0: ChannelIndex, CH1: ChannelIndex, PIO: PIOExt, SM: StateMachineIndex>(
    ch0: Channel<CH0>,
    ch1: Channel<CH1>,
    tx: Tx<(PIO, SM)>,
    buf1: Option<&'static mut [u32; 4]>,
    buf2: Option<&'static mut [u32; 4]>,
) -> Option<DmaTx<CH0, CH1, PIO, SM>> {
    let txt = Config::new((ch0, ch1), buf2?, tx).start();
    Some(txt.read_next(buf1?))
}

pub fn pio_build<PIO: PIOExt, SM: StateMachineIndex>(
    pin_id: u8,
    program: InstalledProgram<PIO>,
    sm: rp_pico::hal::pio::UninitStateMachine<(PIO, SM)>,
) -> (StateMachine<(PIO, SM), Stopped>, Tx<(PIO, SM)>) {
    let (mut sm, _, tx) = PIOBuilder::from_installed_program(program)
        .out_pins(pin_id, 1)
        .clock_divisor_fixed_point(32768, 0)
        .autopull(true)
        .build(sm);
    sm.set_pindirs([(pin_id, PinDir::Output)]);

    (sm, tx)
}
