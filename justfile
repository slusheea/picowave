@flash:
    cargo +nightly run --target thumbv6m-none-eabi

@release:
    cargo +nightly run --release --target thumbv6m-none-eabi

@clippy:
    cargo +nightly clippy -- -w clippy::pedantic -w clippy::nursery

@debug:
    probe-rs debug --chip RP2040 --protocol swd
