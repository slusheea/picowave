from numpy import floor, linspace, pi, sin, tile

# Based on https://en.wikipedia.org/wiki/Pulse-density_modulation#Algorithm
def pdm(input_samples):
    sample_count = len(input_samples)
    output_samples = [0] * sample_count
    running_error = 0

    for i in range(sample_count):
        running_error += input_samples[i]
        if running_error > 0:
            output_samples[i] = 1
        else:
            output_samples[i] = -1
        running_error -= output_samples[i]

    return [1 if i == 1 else 0 for i in output_samples] # Make it into binary

def plot_pdm(wave, pdm_output):
    import matplotlib.pyplot as plot

    plot.plot(0.5 * wave + 0.5, color='red') # Move to y = [0, 1]

    for i in range(len(pdm_output)):
        if pdm_output[i] == 1:
            plot.bar(i, 1, color='blue', width=0.9, alpha=0.5)

    plot.axis('off')
    plot.show()

time = linspace(0, 1, 128, endpoint=False)
wave = sin(2 * pi * time) # sine
# wave = 2 * (time - floor(0.5 + time)) # sawtooth

pdm_output = pdm(wave)

# Prints it out ready to copy-paste into a rust array. The LSBs get shifted out first, so reverse every word.
bit_string = ''.join(str(x) for x in pdm_output)
print(f"    0b{bit_string[:32][::-1]},\n    0b{bit_string[32:64][::-1]},\n    0b{bit_string[64:96][::-1]},\n    0b{bit_string[96:128][::-1]},")
# plot_pdm(wave, pdm_output)
